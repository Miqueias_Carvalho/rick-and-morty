import "./style.css";
import CharCard from "../CharCard";

function Characters({ characterList }) {
  return (
    <div className="container_characters">
      <h2>Meus Personagens</h2>
      <div className="container_personagem">
        {characterList.map((elem) => (
          <CharCard personagem={elem} key={elem.id} />
        ))}
      </div>
    </div>
  );
}
export default Characters;
