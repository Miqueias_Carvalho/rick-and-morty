import "./style.css";
function CharCard({ personagem: { status, name, image, species } }) {
  return (
    <div className={status === "Alive" ? "card" : "card color_not_alive"}>
      <p>{name}</p>
      <img src={image} alt="" className="picture"></img>
      <p>{species}</p>
    </div>
  );
}
export default CharCard;
