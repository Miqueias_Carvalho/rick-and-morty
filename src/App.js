import "./App.css";
import React from "react";
import { useState, useEffect } from "react";
import Characters from "./components/Characters";

function App() {
  const [characterList, setCharacterList] = useState([]);

  useEffect(() => {
    fetch("https://rickandmortyapi.com/api/character")
      .then((response) => response.json())
      .then((response) => setCharacterList(response.results))
      .catch((err) => console.log(err));
  }, []);

  // console.log(characterList);

  return (
    <div className="App">
      <header className="App-header">
        {/* {characterList.map((elem, ind) => (
          <p key={ind}>{elem.name}</p>
        ))} */}
        <Characters characterList={characterList} />
      </header>
    </div>
  );
}

export default App;
